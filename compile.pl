#!/usr/bin/perl -I.
#------------------------------------------------------
=head1 NAME

=head1 SYNOPSIS

=cut
#------------------------------------------------------
#    use Data::Dump qw(dump);
    use File::Find;
    use File::Copy;

use strict;

    use buckd;
#------------------------------------------------------
    package cmp_params;
    use vars qw( $compile_outdir $compile_index_file $compile_server $compile_port);

    $compile_outdir="tftp";
    $compile_index_file="index.gs";
    $compile_server=$settings::MYHOST;
    $compile_port="TFTP:69"; #tftp

    do("compile_settings.pl");
#------------------------------------------------------
    package main;
#------------------------------------------------------
# null
#------------------------------------------------------
sub null {
    my ($par)=@_;

    return $par;
}
#------------------------------------------------------
# sendStr
#------------------------------------------------------
    sub sendStr {
         my ($str)=@_;

         $str =~ s/[\r\n]+$//;

         my ($ds, $sel, $host, $port, $n) = split(/\t/, $str, 5);

         if ($host eq $settings::MYHOST and $port eq $settings::MYPORT)
         {
              $host = $cmp_params::compile_server;
              $port = $cmp_params::compile_port;

              if (-d $settings::DIR."/".$sel)
              {
                  $sel .= "/" unless $sel =~ m{/$};
                  $sel .= $cmp_params::compile_index_file;
              }

              $str=join("\t" ,$ds, $sel, $host, $port, $n);
         }
         $buckd_compile::data .= $str."\r\n";

         return;
    }
#------------------------------------------------------
# save
#------------------------------------------------------
    sub save {
         my ($fileName)=@_;

         open (my $file,">$fileName") or die "cant open '$fileName'";
         binmode $file;

         print $file $buckd_compile::data;

         close $file;

         $buckd_compile::data="";@buckd_compile::lines=(); #free memory

         return;
    }
#------------------------------------------------------
# compileCatalog
#------------------------------------------------------
    sub compileCatalog {
        my ($filename, $fileOut)=@_;

        buckd_compile::init();
        $gopher_dir::sendLine=\&sendStr;

        unless (sysopen(buckd_send::S, $filename, 0)) 
        {
            return 1;
        }
        gopher_dir::sendCatalog();
        close(buckd_send::S);

        save($fileOut);

        return 0;
    }
#------------------------------------------------------
# compileDirectory
#------------------------------------------------------
    sub compileDirectory {
        my ($filename, $fileOut)=@_;

        buckd_compile::init();
        $gopher_dir::sendLine=\&sendStr;

        unless (opendir(buckd_send::D, $filename)) 
        {
            return 1;
        }
        gopher_dir::sendDirectory();
        close(buckd_send::D);

        save($fileOut);

        return 0;
    }
#------------------------------------------------------
# main
#------------------------------------------------------
sub main
{

     my @files;
     ::find(sub {push(@files,$File::Find::name) unless /\.sgbin$/i}, $settings::DIR);

#     print ::dump(\@files),"\n";

     for my $file (@files)
     {
         my $slink=substr($file,length($settings::DIR));
         $request::request=$slink;

         my $outFile= $cmp_params::compile_outdir.$slink;

         print "($slink,$outFile)\n";

         if (-d $file) 
         {
                $buckd::locator=$file;
                $request::request .= "/";
                mkdir($outFile);

                $outFile=$outFile."/".$cmp_params::compile_index_file;

                my $filename=buckd_sgbin_update::findGophermap($file);
                if ($filename)
                {
                     compileCatalog($filename,$outFile);
                }
                else
                {
                     compileDirectory($file,$outFile);
                }

         } 
         elsif (-f $file)
         {
               $buckd::locator=$file;
               my $typeExt=buckd::itypebyext_send($slink);
               if ( $typeExt eq "1")
               {
                     compileCatalog($file,$outFile);
               }
               elsif ( $typeExt eq "0")
               {
                    copy($file,$outFile);
               }
               elsif ( $typeExt eq "sgbin")
               {

               }
               else
               {
                    copy($file,$outFile);
               }
         }
 
     }

     return;
}
#------------------------------------------------------
::main(@ARGV);

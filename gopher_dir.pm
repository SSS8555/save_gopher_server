#------------------------------------------------------
=head1 NAME

=head1 SYNOPSIS

=cut
#------------------------------------------------------
    no warnings 'uninitialized';

    use Fcntl ':mode';
    use POSIX qw(strftime);

    use strict;

    package gopher_dir;
    use vars qw( @postfix);

#    @postfix=('B', 'KiB', 'MiB', 'GiB', 'TiB', 'PiB', 'EiB', 'ZiB', 'YiB');
    @postfix=('.B', 'Kb', 'Mb', 'Gb', 'Tb', 'Pb', 'Eb', 'Zb', 'Yb');

    use vars qw( $sendLine);

    $sendLine=\&printLine;

#------------------------------------------------------
# null
#------------------------------------------------------
    sub null {
      my ($par)=@_;


      return $par;
    }
#------------------------------------------------------
# printLine
#------------------------------------------------------
    sub printLine {
      my ($str)=@_;

      print $str;

      return;
    }
#------------------------------------------------------
# formatNumber
#------------------------------------------------------
    sub formatNumber {
      my ($n,$maxl)=@_;

#      $n *=$n;

      my $res;
      my $pos=0;
      while(1)
      {
          $res=sprintf("%.1f",$n);
          $res=(0+$res).$postfix[$pos];

          last if length($res)<=$maxl;

          $pos++;
          $n=$n/1024;
      }

      return $res;
    }
#------------------------------------------------------
# main
#------------------------------------------------------
sub main {
    my ($dir, $cols, $fileS) = @_;

    my @contents = read_dir($dir."/", $fileS);
    printList($dir, \@contents, $cols);
}
#------------------------------------------------------
# printGlob
#------------------------------------------------------
    sub printGlob {
      my ($ds,$sel,$host, $port)=@_;

      my $ditype = substr($ds, 0, 1);
      $ditype = "" if ($ditype eq '?');

      $sel=request::canon($sel);

      my @files = grep(!/\.sgbin$/i , glob($settings::DIR . $sel));

      for(my $i=0; $i<@files; $i++)
      {
            my ($fname)=($files[$i] =~ m{/([^/]+)$}); # remove path

            $files[$i]=file_info("",$files[$i]);

            $files[$i]->{name}=$fname;
      }

      (($settings::DIR . $sel) =~ m#/[^/]+$#) && (my $dir = $`);

      printList($dir."/", \@files, 80, $ditype);

      return;
    }
#------------------------------------------------------
# printList
#------------------------------------------------------
sub printList {
    my ($dir, $contents, $cols, $gtype) = @_;

#    print "i($dir, $contents, $cols, $gtype)\n";

    my @sorted   = sort { ($b->{isdir} <=> $a->{isdir})*8 + (lc($a->{name}) cmp lc($b->{name}))  } @$contents;
#    @sorted      = reverse(@sorted) if $order eq 'des';

    my $nameL=($cols-23-2);


    for my $fi (@sorted)
    {
        my $slink = $dir."/".$fi->{name};
        my $itype=buckd::itypebyext_list($slink);

        $itype=$gtype if length($gtype);

        my $name=$fi->{name};
        $name= substr($name,0,$nameL-1).">" if length($name)>$nameL;

        my $flen;
        if ($fi->{isdir})
        {
            $flen="[dir]"
        }
        else
        {
            $flen=formatNumber($fi->{size},7);
        }

        my $str=sprintf("%-".$nameL."s %7s %16s", 
        $name, $flen,  ::strftime('%Y-%m-%d %H:%M', gmtime($fi->{mtime})));

        my $crc32=buckd_send::getCRC32($slink);

        if ($crc32 eq "non")
        {
           $crc32="";
        }
        else
        {
           $crc32="\t=".sprintf("%08X",$crc32).";";
        }


        $slink =~ s/^${settings::DIR}//;
        $slink = request::canon($slink);

#        print "$itype$str\t$slink\t${settings::MYHOST}\t${settings::MYPORT}\r\n";



        &$sendLine("$itype$str\t$slink\t${settings::MYHOST}\t${settings::MYPORT}$crc32\r\n");

    }
}
#------------------------------------------------------
# read_dir
#------------------------------------------------------
sub read_dir {
    # Takes a dir path.
    # Returns a list of file_info() hash refs.
    my ($d, $fileS) = @_;

    my @res=();

    for my $file (readdir($fileS))
    {
        next if ($file eq "." or $file eq "..");
        next if $file =~ /\.sgbin$/i;

        push(@res,file_info($d,$file));

    }

    return @res;
}
#------------------------------------------------------
# file_info
#------------------------------------------------------
sub file_info {
    # Takes a path to a file/dir.
    # Returns hash ref containing the path plus any stat() info you need.
    my ($d,$f)=@_;
    my @s = stat($d.$f);
    return {
        name  => $f,
        mtime => $s[9],
        size => $s[7],
        isdir => ::S_ISDIR($s[2]),
    };
}
#------------------------------------------------------
# sendCatalog
#------------------------------------------------------
    sub sendCatalog {
      my ($fileS)=@_;

      my $dirname=$request::request;
      $dirname =~ s{/[^/]*$}{};

      my $typeCRC=buckd::itypebyext_crc($request::request);

      while(my $s=<$fileS>) {
            chomp($s);
            if ($s =~ /^\./) {
                  $s = " ".$s;
            }
            if (! ($s =~ /\t/) ) {
                  $s ||= " ";
#                  print "i$s\t\terror.host\t1\r\n";
                  &$sendLine("i$s\t\terror.host\t1\r\n");
            } else {
                  my ($ds, $sel, $host, $port, $n) = split(/\t/, $s, 5);

                  $sel = substr($ds, 1) unless (length($sel) || length($host));

                  $sel = "$dirname/$sel" if ($sel !~ m#^/# && !length($host) && $sel !~ /^URL:/ && $sel !~ /^GET/);

                  unless ($sel =~ /^URL:/) 
                  {
                        1 while ($sel =~ s#//#/#);
                  }

                  $host ||= $settings::MYHOST;
                  $port ||= $settings::MYPORT;

                  if ( ($s =~ /^[^\t]+\t$/) ) 
                  {
#                        print STDOUT "dirname=${request::request};printGlob($ds\t$sel\t$host\t$port\r\n";
                        printGlob($ds,$sel,$settings::MYHOST, $settings::MYPORT) unless $typeCRC;
                  } 
                  else 
                  {
#                        print "$ds\t$sel\t$host\t$port\r\n";

                        my $crc32="";

                        if ($host eq $settings::MYHOST)
                        {
                              my $crc32get="non";
                              $crc32get=buckd_send::getCRC32( $settings::DIR.$sel ) unless $typeCRC;

                              if ($crc32get ne "non")
                              {
                                 $crc32="\t=".sprintf("%08X",$crc32get).";";
                              }
                        }
                        
                        &$sendLine("$ds\t$sel\t$host\t$port$crc32\r\n");
                  }
            }
      }

#      print ".\r\n";
      &$sendLine(".\r\n");

      return;

    }
#------------------------------------------------------
# sendDirectory
#------------------------------------------------------
    sub sendDirectory {
      my ($fileS)=@_;

      main($buckd::locator,80,$fileS);

#      print ".\r\n";
      &$sendLine(".\r\n");

      return;

    }
#------------------------------------------------------
1;

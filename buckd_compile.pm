#------------------------------------------------------
=head1 NAME

=head1 SYNOPSIS

=cut
#------------------------------------------------------
    no warnings 'uninitialized';
    use strict;
    use crc32;

    package buckd_compile;
#------------------------------------------------------
#    use vars qw( $fileOut $oldfh $data $dataCRC32);
    use vars qw( $data $dataCRC32 @lines);

#------------------------------------------------------
# null
#------------------------------------------------------
    sub null {
         my ($par)=@_;


         return $par;
    }
#------------------------------------------------------
# selfGet
#------------------------------------------------------
    sub selfGet {

         return [$data,$dataCRC32,[@lines]];
    }
#------------------------------------------------------
# selfSet
#------------------------------------------------------
    sub selfSet {
         my $linesIn;
         ($data,$dataCRC32,$linesIn)=@{$_[0]};

         @lines=@$linesIn;

#         print STDOUT "selfSet($data,$dataCRC32,$linesIn)\n";

         return;
    }
#------------------------------------------------------
# sendStr
#------------------------------------------------------
    sub sendStr {
         my ($str)=@_;

         $data .= $str;

         push(@lines,length($data));

         return;
    }
#------------------------------------------------------
# init
#------------------------------------------------------
    sub init {

         $data="";
         @lines=(0);

         $gopher_dir::sendLine=\&sendStr;

         return;
    }
#------------------------------------------------------
# save
#------------------------------------------------------
    sub save {
         my ($fileName)=@_;

         $dataCRC32=crc32::strcrc32($data);

         open (my $file,">$fileName") or die "cant open '$fileName'";
         binmode $file;


         print $file pack("L",$dataCRC32 );
         print $file pack("L",@lines-1);
         print $file pack("L",length($data));
         print $file $data;

         for my $shift (@lines)
         {
             print $file pack("L",$shift );
         }

         close $file;

         $data="";@lines=(); #free memory

         return;
    }
#------------------------------------------------------
1;

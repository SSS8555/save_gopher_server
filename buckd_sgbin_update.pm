#------------------------------------------------------
=head1 NAME

=head1 SYNOPSIS

=cut
#------------------------------------------------------
    no warnings 'uninitialized';
    use strict;

    use crc32;
    use gopher_dir;

    package buckd_sgbin_update;
#------------------------------------------------------
    use vars qw( $error);

#------------------------------------------------------
# null
#------------------------------------------------------
    sub null {
         my ($par)=@_;


         return $par;
    }
#------------------------------------------------------
# max
#------------------------------------------------------
    sub max {
         my ($a,$b)=@_;

         return  $a > $b ? $a : $b;
    }
#------------------------------------------------------
# compileCatalog
#------------------------------------------------------
    sub compileCatalog {
        my ($filename, $fileOut)=@_;

        $error=0;

        my $fileS;
        unless (sysopen($fileS, $filename, 0)) 
        {
            $error=1;
            return;
        }

        my $saveBC=buckd_compile::selfGet();
        buckd_compile::init();

        gopher_dir::sendCatalog($fileS);
        close($fileS);

        buckd_compile::save($fileOut);

        buckd_compile::selfSet($saveBC);

        return;
    }
#------------------------------------------------------
# compileDirectory
#------------------------------------------------------
    sub compileDirectory {
        my ($filename, $fileOut)=@_;

        $error=0;

        my $fileS;
        unless (opendir($fileS, $filename)) 
        {
            $error=1;
            return;
        }

        my $saveBC=buckd_compile::selfGet();
        buckd_compile::init();

        gopher_dir::sendDirectory($fileS);
        close($fileS);

        buckd_compile::save($fileOut);

        buckd_compile::selfSet($saveBC);

        return;
    }
#------------------------------------------------------
# updateCatalogOut
#------------------------------------------------------
    sub updateCatalogOut {
        my ($filename,$fileOut)=@_;

        $error=0;

        my $mtime=(stat($filename))[9]; # update on change

        unless ($filename =~ m{\.gg$}i)
        {
           $mtime=max($mtime,time()-60*1); # update every 1 min or on change
        }

        if ( $mtime > (stat($fileOut))[9])
        {
            compileCatalog($filename,$fileOut);
            return if $error;
        }

        return;

    }
#------------------------------------------------------
# updateCatalog
#------------------------------------------------------
    sub updateCatalog {
        my ($filename)=@_;

        $error=0;

        my $fileOut=$filename.".sgbin";

        updateCatalogOut($filename,$fileOut);

        return if $error;
    
        return $fileOut;
    }
#------------------------------------------------------
# findGophermap
#------------------------------------------------------
    sub findGophermap {
        my ($dirname)=@_;

        my $filename=$dirname."/gophermap";
        if  ( not -f  $filename)
        {
            $filename=$dirname."/gophermap.gg";
            if  ( not -f  $filename)
            {
                $filename=$dirname."/gophermap.gl";
                if  ( not -f  $filename)
                {
                    $filename=$dirname."/gophermap.gopher";
                    if  ( not -f  $filename)
                    {
                        $filename="";
                    }
                }
            }
        }
        return $filename;
    }
#------------------------------------------------------
# updateDirectory
#------------------------------------------------------
    sub updateDirectory {
        my ($dirname)=@_;

        my $fileOut=$dirname."/.sgbin";

        my $filename=findGophermap($dirname);

        $error=0;
        if ($filename)
        {
             updateCatalogOut($filename, $fileOut);
             return if $error;
        }
        else
        {
             my $mtime=time()-60*1; # update every 1 min

             if ( $mtime > (stat($fileOut))[9] )
             {
#                 print STDOUT "update:$mtime>",(stat($fileOut))[9],"\n";
                 compileDirectory($dirname,$fileOut);
                 return if $error;
             }
        }

        return $fileOut;
    }
#------------------------------------------------------
# save_text
#------------------------------------------------------
    sub save_text {
         my ($fileName,$crc32,$lenData,$lines)=@_;

         open (my $file,">$fileName") or die "cant open '$fileName'";
         binmode $file;

         print $file pack("L",$crc32 );
         print $file pack("L",@$lines-1);
         print $file pack("L",$lenData);
#         print file $data;

         for my $shift (@$lines)
         {
             print $file pack("L",$shift );
         }


         close $file;

         return;
    }
#------------------------------------------------------
# compileText
#------------------------------------------------------
    sub compileText {
        my ($filename,$fileOut)=@_;

        $error=0;

        my $file;
        unless (sysopen($file, $filename, 0)) 
        {
            $error=1;
            return;
        }
        binmode($file);

        my @lines=(0);
        crc32::init();
        my $State=0;
        while(!eof($file)) 
        {
              my $b=getc($file);

              crc32::addStr($b);


              if ($State==0) # in string
              {
                   if (ord($b)==13 )
                   {
                       $State=1;

                   }
                   elsif (ord($b)==10)
                   {
                       push(@lines,tell($file));
                       $State=0;
                   }
              }
              elsif ($State==1) # afrer 13
              {
                   if (ord($b)==13 )
                   {
                       $State=1;
                       push(@lines,tell($file)-1);
                       push(@lines,tell($file));
                   }
                   elsif (ord($b)==10)
                   {
                       $State=0;
                       push(@lines,tell($file));
                   }
                   else
                   {
                       $State=0;
                       push(@lines,tell($file)-1);
                   }
              }
        }

        my $lenData=tell($file);

        close($file);

        push(@lines,$lenData) if ($lenData>$lines[$#lines]);

        save_text($fileOut,crc32::result(),$lenData,\@lines);
 
        return;
    }
#------------------------------------------------------
# updateText
#------------------------------------------------------
    sub updateText {
        my ($filename)=@_;

        my $fileOut=$filename.".sgbin";

        if ( (stat($filename))[9] > (stat($fileOut))[9] )
        {
            compileText($filename,$fileOut);
            return if $error;
        }
 
        return $fileOut;
    }
#------------------------------------------------------
# updateBin
#------------------------------------------------------
    sub updateBin {
        my ($filename)=@_;

        $error=0;

        my $fileOut=$filename.".sgbin";

        if ( (stat($filename))[9] > (stat($fileOut))[9] )
        {
             unless (sysopen(buckd_send::S, $filename, 0)) 
             {
                 $error=1;
                 return;
             }
             binmode(buckd_send::S);

             crc32::init();
             while(!eof(buckd_send::S)) 
             {
                   my $b="";
                   read(buckd_send::S, $b, 16384);
                   crc32::addStr($b);
             }

             my $lenData=tell(buckd_send::S);

             close(buckd_send::S);

             save_text($fileOut,crc32::result(),$lenData,[0,$lenData]);
        }

 
        return $fileOut;
    }
#------------------------------------------------------
1;

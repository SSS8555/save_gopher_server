#------------------------------------------------------
=head1 NAME

=head1 SYNOPSIS

=cut
#------------------------------------------------------
    use Data::Dump qw(dump);

    no warnings 'uninitialized';
    use strict;

    use buckd_send_g6;
    use buckd_data;
    use request;

    use buckd_compile;
    use buckd_sgbin_update;
    use buckd_sgbin_send;
    use buckd_send;


    package settings;
#------------------------------------------------------
    use vars qw( $DIR $MYHOST $MYPORT $LOG $SERVER $server $port $DIR_FORM);

    $DIR = "./data";        # data dir
    $DIR_FORM = "./form";   # dir for save FORM data
    $LOG = "./access.log";  # log file
    $MYHOST = "127.0.0.1";  # external (host) adress
    $MYPORT = "70";         # external port
    $SERVER = "";           # server name
    $server = $MYHOST;      # bind adress
    $port   = $MYPORT;      # bind port

    do("settings.pl");

#------------------------------------------------------
    package buckd;

    use vars qw( $locator );
    use vars qw($REMOTE_HOST $REMOTE_PORT $REMOTE_ADDR);
#------------------------------------------------------
# null
#------------------------------------------------------
    sub null {
      my ($par)=@_;


      return $par;
    }
#------------------------------------------------------
# sock_to_host
#------------------------------------------------------
sub sock_to_host {
      my($sock) = (@_);

      return (undef, undef, undef) if (!$sock);
      my($sockaddr) = 'S n a4 x8';
      my($AFC, $port, $thataddr, $zero) = unpack($sockaddr, $sock);
      my($ip) = join('.', unpack("C4", $thataddr));
      my($af) = 2;

      my($hn) = $ip;

      return ($hn, $port, $ip);
}

#------------------------------------------------------
# serv
#------------------------------------------------------
    sub serv {
      local (*DATAS)=@_;

#      ($ENV{'REMOTE_HOST'}, $ENV{'REMOTE_PORT'}, $ENV{'REMOTE_ADDR'}) = sock_to_host(getpeername(DATAS));
      ($REMOTE_HOST, $REMOTE_PORT, $REMOTE_ADDR) = sock_to_host(getpeername(DATAS));

      select(DATAS); $|++;

      request::readRequest(*DATAS);

      $locator = $settings::DIR.$request::request;

      print STDOUT "[${request::request},${request::gplus},${request::dinfo},${request::gopher6},$locator]\n";
      
      if ($request::gplus) { # this is for UMN gopher

            buckd_data::nonPlus();

            buckd_log::logStr(300, "forced g+ redirect");
            return;
      }

# quux.org/0/Archives/Mailing Lists/gopher/gopher.2002-02?/MBOX-MESSAGE/34
      if ($request::request =~ s#^/?URL:([^:/]+:/?/?)#$1#) {
#        		$request =~ s/\t/?/;

            buckd_data::wrongURL();

            buckd_log::logStr(301, "URL redirect");
            return;
      }

      sendResponse();

      return;
    }
#------------------------------------------------------
# sendResponse
#------------------------------------------------------
    sub sendResponse {

         my $len;
         if (-d $locator) 
         {
               $request::request .= "/" unless $request::request =~ m{/$}; 

               $len=buckd_send::sendDirectory($locator);

               buckd_log::logStr(221, $len, "directory") if $len>0;
         } 
         elsif (-f $locator)
         {
               my $typeExt=itypebyext_send($locator);
               if ( $typeExt eq "1")
               {
                    $len=buckd_send::sendCatalog($locator);
                    buckd_log::logStr(201, $len, "catalog .gopher") if $len>0;
               }
               elsif ( $typeExt eq "0")
               {
                    $len=buckd_send::sendText($locator);
                    buckd_log::logStr(200, $len, "text") if $len>0;
               }
               elsif ( $typeExt eq "sgbin")
               {
                    $len=-1;
               }
               else
               {
                    $len=buckd_send::sendBin($locator);
                    buckd_log::logStr(209, $len, "bin") if $len>0;
               }
         }
         else
         {
              buckd_send_g6::errorr(104, "doesn't exist", "This resource cannot be located.");
              return;
         }

         if ($len<0)
         {
              buckd_send_g6::errorr(103, "isn't accessible to you" , "This resource's permissions do not permit you to read it.");
         }

         return;
    }
#------------------------------------------------------
# itypebyext_crc
#------------------------------------------------------
sub itypebyext_crc {
      my ($xentr) = (@_);

      my $itype =
         ($xentr =~ m{\.FG6.$}i) ? 1 : 
         ($xentr =~ m{\.gg$}i) ? 1 : 
         0;

      return $itype;
}
#------------------------------------------------------
# itypebyext_send
#------------------------------------------------------
sub itypebyext_send {
      my ($xentr) = (@_);
      my $itype =
            (-d $xentr) ? "1" :
            ($xentr =~ /\.sgbin$/i) ? "sgbin" :
            ($xentr =~ /\.gopher$/i) ? "1" :
            ($xentr =~ /\.gg$/i) ? "1" :
            ($xentr =~ /\.gl$/i) ? "1" :
            ($xentr =~ /\.gs$/i) ? "0" :
            ($xentr =~ /\.txt$/i) ? "0" :
            ($xentr =~ /\.css$/i) ? "0" :
            ($xentr =~ /\.xml$/i) ? "0" :
            ($xentr =~ /\.html?$/i) ? "0" :

            ($xentr =~ m{\.S?G6E$}i) ? "9" :
            ($xentr =~ m{\.SG6.$}i) ? "0" :
            ($xentr =~ m{\.FG6.$}i) ? "1" :
            ($xentr =~ m{\.G6.$}i) ? "1" :

            (-B $xentr) ? "9" :
      "9";

      return $itype;
}
#------------------------------------------------------
# itypebyext_list
#------------------------------------------------------
sub itypebyext_list {
      my ($xentr) = (@_);

      my ($itype)=($xentr =~ m{\.[SF]?G6(.)$}i);

      return uc($itype) if $itype;

      $itype =
            (-d $xentr) ? "1" :
            ($xentr =~ /\.sgbin$/i) ? "sgbin" :
            ($xentr =~ /\.gopher$/i) ? "1" :
            ($xentr =~ /\.gg$/i) ? "1" :
            ($xentr =~ /\.gl$/i) ? "1" :
            ($xentr =~ /\.gs$/i) ? "1" :  # never processed
            ($xentr =~ /\.txt$/i) ? "0" :
            ($xentr =~ /\.gif$/i) ? "g" :
            ($xentr =~ /\.gz$/i) ? "9" :
            ($xentr =~ /\.zip$/i) ? "5" :
            ($xentr =~ /\.jpe?g$/i) ? "I" :
            ($xentr =~ /\.png$/i) ? "p" :
            ($xentr =~ /\.pdf$/i) ? "d" :
            ($xentr =~ /\.css$/i) ? "c" :
            ($xentr =~ /\.xml$/i) ? "x" :
            ($xentr =~ /\.html?$/i) ? "0" :
            ($xentr =~ /\.hqx$/i) ? "4" :
            (-B $xentr) ? "9" :
      "9";

      return $itype;
}
#------------------------------------------------------
1;

#------------------------------------------------------
=head1 NAME

=head1 SYNOPSIS

=cut
#------------------------------------------------------
    no warnings 'uninitialized';
    use strict;

    package buckd_send_g6;
#------------------------------------------------------
#    use vars qw( $fileOut $oldfh $data $dataCRC32);
#    use vars qw( $data $dataCRC32);

#------------------------------------------------------
# null
#------------------------------------------------------
    sub null {
         my ($par)=@_;


         return $par;
    }
#------------------------------------------------------
# packNumber
#------------------------------------------------------
    sub packNumber {
         my ($dataLen)=@_;

         my $varB=0;
         $varB=1 if $dataLen>=65536;
         $varB=2 if $dataLen>=65536*65536;
         $varB=3 if $dataLen < 0;

         my $dataLenPack;
         if ($varB == 0)
         {
            $dataLenPack= pack("S",$dataLen );
         }
         elsif ($varB == 1)
         {
            $dataLenPack= pack("L",$dataLen );
         }
         elsif ($varB == 2)
         {
            $dataLenPack= pack("Q",$dataLen );
         }
         elsif ($varB == 3)
         {
            $dataLenPack= "";
         }

         return ($varB,$dataLenPack);
    }
#------------------------------------------------------
# sendReturnCode
#------------------------------------------------------
    sub sendReturnCode {
         my ($dataLen,$gtype,$dataCRC32,$linesN,$dataSize)=@_;

         return 1 unless ($request::gopher6);


      # varA: 0-CRC not found; 1-CRC found; 2 - CRC unknown
      # varB: 0-length <65536 (16 bit); 1-length >65536 (32 bit); 2-length >65536^2 (64 bit); 3-chanked encoding with chank length <65536 (16 bit)
      # returnCode=varB*4+varA

         print pack("L",$request::g6crc32 );

         my $varA=0;

         if ($dataCRC32 eq "non")
         {
             $varA=2;
         }
         else
         {
             for my $c (@request::g6CRCs)
             {
                 $varA=1 if $c == $dataCRC32;
             }
         }


         my ($varB,$dataLenPack)=packNumber($dataLen);

         print STDOUT "varB=$varB;varA=$varA;\n";

         print pack("C",$varB*4+$varA);   # no error

         print pack("A",$gtype );


      # 1  - send CRC if CRC found
      # 2  - send CRC if CRC not found
      # 0  - does nothing
      # 3  - send DATA if CRC found
      # 4  - send DATA if CRC not found
      # varA: 0-CRC not found; 1-CRC found; 2 - CRC unknown (same like not found, but CRC never sends)

         if ($varA==1)
         {
            print pack("L",$dataCRC32) if $request::g6flags[1]; # 1  - send CRC if CRC found
         }
         elsif ($varA==0)
         {
            print pack("L",$dataCRC32) if $request::g6flags[1]; # 2  - send CRC if CRC not found
         }
         elsif ($varA==2) #  varA: 2 - CRC unknown (but CRC never sends) 
         {

         }

         my $sendFlag=0;
         if ($varA==1)
         {
            if ($request::g6flags[3]) # 3  - send DATA if CRC found
            {
                 print $dataLenPack ;
                 $sendFlag=1;
            }
         }
         elsif ($varA==0)
         {
            if ($request::g6flags[4]) # 4  - send DATA if CRC not found
            {
                 print $dataLenPack ;
                 $sendFlag=1;
            }
         }
         elsif ($varA==2) #  varA: 2 - CRC unknown (same like not found) 
         {
            if ($request::g6flags[4]) # 4  - send DATA if CRC not found
            {
                 print $dataLenPack ;
                 $sendFlag=1;
            }
         }

      # 5  - send overall number of lines in file (or varB=3 of unknown)
         if ($request::g6flags[5]) 
         {
              my ($var,$dataLenPack)=packNumber($linesN);
              print pack("C",$var).$dataLenPack ;
         }
      # 6  - send overall number of bytes in file (or varB=3 of unknown)
         if ($request::g6flags[6]) 
         {
              my ($var,$dataLenPack)=packNumber($dataSize);
              print pack("C",$var).$dataLenPack ;
         }

         return $sendFlag;
    }
#------------------------------------------------------
# errorr
#------------------------------------------------------
sub errorr {
      my ($code, $emsg1, @stuff) = (@_);

      buckd_log::logStr($code, 0, $emsg1);

      if ($request::gopher6)
      {
          print pack("L",$request::g6crc32 );
          print pack("C",$code );
      }
      else
      {
          print  "3 '${request::request}' $emsg1!\t\terror.host\t1\r\n";
          for my $s (@stuff) 
          {
                print "i$s\t\terror.host\t1\r\n";
          }
          print ".\r\n";
      }

      return;
}
#------------------------------------------------------
1;
